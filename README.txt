

Description
-------------

The views tablesorter module is a views style plugin that can be used to output
views in a table that uses the tablesorter jQuery plugin.

The tablesorter jQuery plugin allows tablesorting without refreshing the page
or re-querying the database.
It also includes a pager so that multiple pages of results can be accurately
sorted without refreshing the page or re-querying the database.

For more tablesorter information see - http://tablesorter.com/docs/


Requirements
--------------

Views 2 is required for this module to be of any use.


Installation
--------------

Install as usual, see http://drupal.org/node/70151 for further information.


Configuration
---------------
There are some main settings at
  Administer > Site configuration > Views tablesorter settings

These settings are:
* Date field sort format:
    Set this to the date format you use on your site.  The tablesorter will
    be expecting dates of this format so if it isn't correct the sort will not
    work properly.
* Pager settings - Rows per page:
    When using the pager the user can select how many rows per page to display.
    The options you select here will be the options the users will have to
    select from. In addition to these options will be the number entered for
    the "Items per page" views option.
* Pager settings - Current page divider:
    When using the pager the current page will be displayed with the pager.
    It will be something like "Page 2 of 3".  This option sets what divides
    the two numbers.


To create a tablesorter view:
* In the "Basic settings" of the view edit screen select "Tablesorter" for the style.
You will then be presented with settings for the table, which are similar to the
settings for the normal table style.

* The main differencences are for the default sort settings.
The default sort can be on multiple columns, so the default priority controls whether
the column is sorted first, second, third etc. and the default order controls whether
the column is sorted ascending or descending.

NOTE: If you have checked the "Disable javascript with Views" option on the views
tools page this module views style will not work as it requires javascript.


Authors/maintainers
---------------------

Author/maintainer:
- Reuben Turk (rooby) - http://drupal.org/user/350381

tablesorter jQuery plugin author:
- Christian Bach


Support / Bugs / Feature requests
-----------------------------------

Issues should be posted in the issue queue on drupal.org:
http://drupal.org/project/issues/views_tablesorter
