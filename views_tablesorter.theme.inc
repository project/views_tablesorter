<?php

/**
 * @file views_tablesorter.theme.inc
 *
 * Theme functions for the views_tablesorter module.
 */

/**
 * Theme the tablesorter pager.
 *
 * @param $view
 *   The view object.
 * @param $size_options
 *   An array of the possible options for items per page.
 *
 * @return
 *   A string of the HTML markup for the tablesorter pager.
 *
 * @ingroup themeable
 */
function theme_views_tablesorter_pager($view, $size_options) {
  $path = drupal_get_path('module', 'views_tablesorter');
  $current_page = '<label class="pagedisplay"></label>';

  $output = '<div id="tablesorter-pager">';
  $output .= '<img class="first" src="' . $path . '/images/first.png"/>';
  $output .= '<img class="prev" src="' . $path . '/images/prev.png"/>';
  $output .= theme('views_tablesorter_current_page', $current_page, $view->total_rows);
  $output .= '<img class="next" src="' . $path . '/images/next.png"/>';
  $output .= '<img class="last" src="' . $path . '/images/last.png"/>';
  $output .= '<div class="size-option">';
  $output .= '<label>' . t('Results per page: ') . '</label>';
  $output .= '<select class="pagesize">';
  foreach ($size_options as $size_option) {
    $output .= '<option value="' . $size_option . '">' . $size_option . '</option>';
  }
  $output .= '</select>';
  $output .= '</div>';
  $output .= '</div>';

  return $output;
}

/**
 * Theme the current page part of the tablesorter pager.
 *
 * @param $current_page
 *   The current page output.  For example it could be "2 of 3".
 *   The actual value of the variable is static HTML, which will then be
 *   replaced by the numbers using jQuery.
 * @param $total_rows
 *   The total number of rows for the view.
 *
 * @return
 *   A string of the HTML markup for the current page part of the pager.
 *
 * @ingroup themeable
 */
function theme_views_tablesorter_current_page($current_page, $total_rows) {
  $output = '<span class="current-page">';
  $output .= t('Page ') . $current_page . t(' (@total_rows records)', array('@total_rows' => $total_rows));
  $output .= '</span>';
  return $output;
}

/**
 * Theme the form for the tablesorter style plugin.
 *
 * @param $form
 *   The form array for the tablesorter style setings form.
 *
 * @return
 *   A string of the HTML markup for the tablesorter style setings form.
 *
 * @ingroup themeable
 */
function theme_views_tablesorter_style_plugin_tablesorter($form) {
  $output = '';
  if ($form['error_markup']) {
    $output .= drupal_render($form['error_markup']);
  }
  $output .= drupal_render($form['description_markup']);

  $header = array(
    t('Field'),
    t('Column'),
    t('Separator'),
    array(
      'data' => t('Sortable'),
      'align' => 'center',
    ),
    t('Default priority'),
    t('Default order'),
  );
  $rows = array();
  foreach (element_children($form['columns']) as $id) {
    $row = array();
    $row[] = drupal_render($form['info'][$id]['name']);
    $row[] = drupal_render($form['columns'][$id]);
    $row[] = drupal_render($form['info'][$id]['separator']);
    if (!empty($form['info'][$id]['sortable'])) {
      $row[] = array(
        'data' => drupal_render($form['info'][$id]['sortable']),
        'align' => 'center',
      );
      $row[] = drupal_render($form['info'][$id]['default_priority']);
      $row[] = drupal_render($form['info'][$id]['default_order']);
    }
    else {
      $row[] = '';
      $row[] = '';
      $row[] = '';
    }
    $rows[] = $row;
  }

  $output .= theme('table', $header, $rows);
  $output .= drupal_render($form);
  return $output;
}
