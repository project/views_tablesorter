
/**
 * This is where we add the tablesorter functionality to the views table.
 */

$(document).ready(function() {
  // Call the tablesorter plugin.
  $('table.views-tablesorter').tablesorter({
    sortList: Drupal.settings.views_tablesorter.tablesorter_settings.default_sort,
    headers: Drupal.settings.views_tablesorter.tablesorter_settings.unsortable_cols,
    dateFormat: Drupal.settings.views_tablesorter.tablesorter_settings.date_format,
    textExtraction: 'complex',
    cssHeader: 'header-sortable',
    cssAsc: 'header-sort-asc',
    cssDesc: 'header-sort-desc',
    widthFixed: Drupal.settings.views_tablesorter.tablesorter_settings.width_fixed,
    widgets: ['zebra', 'columnHighlight']
  });
  if (Drupal.settings.views_tablesorter.use_pager) {
    // Add the tablesorter pager plugin.
    $('table.views-tablesorter').tablesorterPager({
      container: $("#tablesorter-pager"),
      positionFixed: false,
      size: Drupal.settings.views_tablesorter.pager_settings.pager_size,
      seperator: Drupal.settings.views_tablesorter.pager_settings.current_page_separator
    });
  }
});
